# Even Backend

## Stack
Golang + gqlgen + Graphql (apollo federation) + gateway (nodejs + express)

## Stage : Production
This code is ready for deployment on AWS Lambda, not usable on local machine

## Deployment details
[This is the link](https://hfsc3cz8f9.execute-api.eu-south-1.amazonaws.com/dev/graphql),
for more info on Makefile,serverless.yml and dotenv ask @naseem181997
