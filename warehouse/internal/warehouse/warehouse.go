package warehouse

import (
	"context"
	"fmt"
	"log"
	"reflect"

	driver "github.com/arangodb/go-driver"
	"github.com/naseem181997/even_backend/warehouse/graph/model"
	database "github.com/naseem181997/even_backend/warehouse/internal/pkg/db/arangodb"
)

func SaveProduct(ctx context.Context, product map[string]interface{}) string {
	col, err := database.DBSession.Collection(ctx, "products")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.CreateDocument(ctx, product)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Created document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}

func UpdateProduct(ctx context.Context, product map[string]interface{}) string {
	var oldProd *map[string]interface{} = &map[string]interface{}{}
	(*oldProd)["Pkey"] = product["Pkey"].(string)
	col, err := database.DBSession.Collection(ctx, "products")
	if err != nil {
		log.Fatal(err)
	}
	product["Pkey"] = nil
	product = deleteNilInterface(product)
	meta, err := col.UpdateDocument(driver.WithReturnOld(ctx, oldProd), (*oldProd)["Pkey"].(string), product)
	if err != nil {
		log.Fatal(err)
	}
	temp_col, err := database.DBSession.Collection(ctx, "temp_products")
	if err != nil {
		log.Fatal(err)
	}
	oldProd = cleanArangoDBMeta(oldProd)
	(*oldProd) = deleteNilInterface(*oldProd)
	temp_meta, err := temp_col.CreateDocument(ctx, oldProd)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Saved document '%s' with key '%s', revision '%s' in temporal table\n", (*oldProd)["Pkey"], temp_meta.Key, meta.Rev)
	fmt.Printf("Updated document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}

func DeleteProduct(ctx context.Context, key model.KeyInput) string {
	var oldProd *map[string]interface{} = &map[string]interface{}{}
	col, err := database.DBSession.Collection(ctx, "products")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.RemoveDocument(driver.WithReturnOld(ctx, oldProd), key.Key)
	if err != nil {
		log.Fatal(err)
	}
	(*oldProd)["Pkey"] = &key.Key
	(*oldProd)["Isdeleted"] = true
	temp_col, err := database.DBSession.Collection(ctx, "temp_products")
	if err != nil {
		log.Fatal(err)
	}
	oldProd = cleanArangoDBMeta(oldProd)
	(*oldProd) = deleteNilInterface(*oldProd)
	temp_meta, err := temp_col.CreateDocument(ctx, oldProd)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Saved document '%s' with key '%s', revision '%s' in temporal table\n", (*oldProd)["Pkey"], temp_meta.Key, meta.Rev)
	fmt.Printf("Deleted document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}
func GetAllProducts(ctx context.Context, AccountKey string) []map[string]interface{} {
	var products []map[string]interface{}
	query := `FOR prod IN products
				FILTER prod.AccountKey == @accountkey
				return prod`
	bindVars := map[string]interface{}{
		"accountkey": AccountKey,
	}
	cursor, err := database.DBSession.Query(ctx, query, bindVars)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close()
	for {
		product := make(map[string]interface{})
		meta, err := cursor.ReadDocument(ctx, &product)
		product["Key"] = &meta.Key
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
		products = append(products, product)
	}

	return products
}
func GetProductByKey(ctx context.Context, AccountKey string, key *string) map[string]interface{} {
	var product map[string]interface{}
	col, err := database.DBSession.Collection(ctx, "products")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.ReadDocument(ctx, *key, product)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
	return product
}

func SaveSupplier(ctx context.Context, supplier map[string]interface{}) string {
	col, err := database.DBSession.Collection(ctx, "suppliers")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.CreateDocument(ctx, supplier)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Created document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}

func UpdateSupplier(ctx context.Context, supplier map[string]interface{}) string {
	var oldSupp *map[string]interface{} = &map[string]interface{}{}
	(*oldSupp)["Skey"] = supplier["Skey"].(string)
	col, err := database.DBSession.Collection(ctx, "suppliers")
	if err != nil {
		log.Fatal(err)
	}
	supplier["Skey"] = nil
	supplier = deleteNilInterface(supplier)
	meta, err := col.UpdateDocument(driver.WithReturnOld(ctx, oldSupp), (*oldSupp)["Skey"].(string), supplier)
	if err != nil {
		log.Fatal(err)
	}
	temp_col, err := database.DBSession.Collection(ctx, "temp_suppliers")
	if err != nil {
		log.Fatal(err)
	}
	oldSupp = cleanArangoDBMeta(oldSupp)
	(*oldSupp) = deleteNilInterface(*oldSupp)
	temp_meta, err := temp_col.CreateDocument(ctx, oldSupp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Saved document '%s' with key '%s', revision '%s' in temporal table\n", (*oldSupp)["Skey"], temp_meta.Key, meta.Rev)
	fmt.Printf("Updated document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}

func DeleteSupplier(ctx context.Context, key model.KeyInput) string {
	var oldSupp *map[string]interface{} = &map[string]interface{}{}
	col, err := database.DBSession.Collection(ctx, "suppliers")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.RemoveDocument(driver.WithReturnOld(ctx, oldSupp), key.Key)
	if err != nil {
		log.Fatal(err)
	}
	(*oldSupp)["Skey"] = &key.Key
	(*oldSupp)["Isdeleted"] = true
	temp_col, err := database.DBSession.Collection(ctx, "temp_suppliers")
	if err != nil {
		log.Fatal(err)
	}
	oldSupp = cleanArangoDBMeta(oldSupp)
	(*oldSupp) = deleteNilInterface(*oldSupp)
	temp_meta, err := temp_col.CreateDocument(ctx, oldSupp)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Saved document '%s' with key '%s', revision '%s' in temporal table\n", (*oldSupp)["Skey"], temp_meta.Key, meta.Rev)
	fmt.Printf("Deleted document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}
func GetAllSuppliers(ctx context.Context, AccountKey string) []map[string]interface{} {
	var suppliers []map[string]interface{}
	query := `FOR supp IN suppliers
				FILTER supp.AccountKey == @accountkey
				return supp`
	bindVars := map[string]interface{}{
		"accountkey": AccountKey,
	}
	cursor, err := database.DBSession.Query(ctx, query, bindVars)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close()
	for {
		supplier := make(map[string]interface{})
		meta, err := cursor.ReadDocument(ctx, &supplier)
		supplier["Key"] = &meta.Key
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
		suppliers = append(suppliers, supplier)
	}

	return suppliers
}

func GetSupplierByKey(ctx context.Context, AccountKey string, key *string) map[string]interface{} {
	var supplier map[string]interface{}
	col, err := database.DBSession.Collection(ctx, "suppliers")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.ReadDocument(ctx, *key, supplier)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
	return supplier
}

func IsNilFixed(i interface{}) bool {
	if i == nil {
		return true
	}
	switch reflect.TypeOf(i).Kind() {
	case reflect.Ptr, reflect.Map, reflect.Array, reflect.Chan, reflect.Slice:
		return reflect.ValueOf(i).IsNil()
	}
	return false
}
func cleanArangoDBMeta(i *map[string]interface{}) *map[string]interface{} {
	(*i)["_key"] = nil
	(*i)["_id"] = nil
	(*i)["_rev"] = nil
	return i
}
func deleteNilInterface(i map[string]interface{}) map[string]interface{} {
	for key, value := range i {
		if IsNilFixed(value) {
			fmt.Printf("deleting %v=>%v\n", key, value)
			delete(i, key)
		}
	}
	return i
}
