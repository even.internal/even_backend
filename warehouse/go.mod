module github.com/naseem181997/even_backend/warehouse

go 1.16

require (
	github.com/99designs/gqlgen v0.13.0 // indirect
	github.com/arangodb/go-driver v0.0.0-20210304082257-d7e0ea043b7f
	github.com/auth0/go-jwt-middleware v1.0.0 // indirect
	github.com/aws/aws-lambda-go v1.23.0
	github.com/awslabs/aws-lambda-go-api-proxy v0.10.0
	github.com/codegangsta/negroni v1.0.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/form3tech-oss/jwt-go v3.2.2+incompatible // indirect
	github.com/gorilla/mux v1.8.0
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/urfave/negroni v1.0.0 // indirect
	github.com/vektah/gqlparser/v2 v2.1.0 // indirect
)
