package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"github.com/mitchellh/mapstructure"
	"github.com/naseem181997/even_backend/warehouse/graph/generated"
	"github.com/naseem181997/even_backend/warehouse/graph/model"
	"github.com/naseem181997/even_backend/warehouse/internal/auth"
	"github.com/naseem181997/even_backend/warehouse/internal/warehouse"
)

func (r *entityResolver) FindProductByKey(ctx context.Context, key *string) (*model.Product, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.Product{}, fmt.Errorf("access denied")
	}
	var resultProd *model.Product
	dbProd := warehouse.GetProductByKey(ctx, accountKey, key)
	err := mapstructure.Decode(dbProd, &resultProd)
	if err != nil {
		panic(err)
	}
	return resultProd, nil
}

func (r *entityResolver) FindSupplierByKey(ctx context.Context, key *string) (*model.Supplier, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.Supplier{}, fmt.Errorf("access denied")
	}
	var resultSupplier *model.Supplier
	dbSupplier := warehouse.GetSupplierByKey(ctx, accountKey, key)
	err := mapstructure.Decode(dbSupplier, &resultSupplier)
	if err != nil {
		panic(err)
	}
	return resultSupplier, nil
}

// Entity returns generated.EntityResolver implementation.
func (r *Resolver) Entity() generated.EntityResolver { return &entityResolver{r} }

type entityResolver struct{ *Resolver }
