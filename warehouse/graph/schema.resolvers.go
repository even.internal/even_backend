package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/naseem181997/even_backend/warehouse/graph/generated"
	"github.com/naseem181997/even_backend/warehouse/graph/model"
	"github.com/naseem181997/even_backend/warehouse/internal/auth"
	"github.com/naseem181997/even_backend/warehouse/internal/warehouse"
)

func (r *mutationResolver) CreateProduct(ctx context.Context, input model.NewProduct) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	var dbProduct map[string]interface{}
	err := mapstructure.Decode(input, &dbProduct)
	if err != nil {
		panic(err)
	}
	dbProduct["AccountKey"] = accountKey
	dbProduct["Isdeleted"] = false
	dbProduct["TransactionDT"] = int(time.Now().Unix())
	Key := warehouse.SaveProduct(ctx, dbProduct)
	return &model.ReturnKey{Key: Key}, nil
}

func (r *mutationResolver) UpdateProduct(ctx context.Context, input model.ProductInput) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	var dbProduct map[string]interface{}
	err := mapstructure.Decode(input, &dbProduct)
	if err != nil {
		panic(err)
	}
	dbProduct["AccountKey"] = accountKey
	dbProduct["Isdeleted"] = false
	dbProduct["TransactionDT"] = int(time.Now().Unix())
	Key := warehouse.UpdateProduct(ctx, dbProduct)
	return &model.ReturnKey{Key: Key}, nil
}

func (r *mutationResolver) DeleteProduct(ctx context.Context, input model.KeyInput) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	Key := warehouse.DeleteProduct(ctx, input)
	return &model.ReturnKey{Key: Key}, nil
}

func (r *mutationResolver) CreateSupplier(ctx context.Context, input model.NewSupplier) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	var dbSupplier map[string]interface{}
	err := mapstructure.Decode(input, &dbSupplier)
	if err != nil {
		panic(err)
	}
	dbSupplier["AccountKey"] = accountKey
	dbSupplier["Isdeleted"] = false
	dbSupplier["TransactionDT"] = int(time.Now().Unix())
	Key := warehouse.SaveSupplier(ctx, dbSupplier)
	return &model.ReturnKey{Key: Key}, nil
}

func (r *mutationResolver) UpdateSupplier(ctx context.Context, input model.SupplierInput) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	var dbSupplier map[string]interface{}
	err := mapstructure.Decode(input, &dbSupplier)
	if err != nil {
		panic(err)
	}
	dbSupplier["AccountKey"] = accountKey
	dbSupplier["Isdeleted"] = false
	dbSupplier["TransactionDT"] = int(time.Now().Unix())
	Key := warehouse.UpdateSupplier(ctx, dbSupplier)
	return &model.ReturnKey{Key: Key}, nil
}

func (r *mutationResolver) DeleteSupplier(ctx context.Context, input model.KeyInput) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	Key := warehouse.DeleteSupplier(ctx, input)
	return &model.ReturnKey{Key: Key}, nil
}

func (r *queryResolver) Suppliers(ctx context.Context) ([]*model.Supplier, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return []*model.Supplier{}, fmt.Errorf("access denied")
	}
	var resultSuppliers []*model.Supplier
	dbSuppliers := warehouse.GetAllProducts(ctx, accountKey)
	err := mapstructure.Decode(dbSuppliers, &resultSuppliers)
	if err != nil {
		panic(err)
	}
	return resultSuppliers, nil
}

func (r *queryResolver) Products(ctx context.Context) ([]*model.Product, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return []*model.Product{}, fmt.Errorf("access denied")
	}
	var resultProds []*model.Product
	dbProds := warehouse.GetAllProducts(ctx, accountKey)
	err := mapstructure.Decode(dbProds, &resultProds)
	if err != nil {
		panic(err)
	}
	return resultProds, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
