package main

import (
	"context"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	negroniadapter "github.com/awslabs/aws-lambda-go-api-proxy/negroni"
	"github.com/gorilla/mux"
	"github.com/naseem181997/even_backend/warehouse/graph"
	"github.com/naseem181997/even_backend/warehouse/graph/generated"
	"github.com/naseem181997/even_backend/warehouse/internal/auth"
	database "github.com/naseem181997/even_backend/warehouse/internal/pkg/db/arangodb"
	"github.com/urfave/negroni"
)

var negroniAdapter *negroniadapter.NegroniAdapter

func init() {
	r := mux.NewRouter()
	ctx := context.Background()

	//database.Migrate()

	n := negroni.New(negroni.HandlerFunc(auth.Middleware().HandlerWithNext), auth.CheckScope(ctx))
	database.InitDB(ctx)
	schema := generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}})
	server := handler.NewDefaultServer(schema)
	r.Handle("/", server)
	n.UseHandler(r)

	negroniAdapter = negroniadapter.New(n)

}

func Handler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	return negroniAdapter.ProxyWithContext(ctx, req)
}

func main() {
	lambda.Start(Handler)
}
