module github.com/naseem181997/even_backend/accounts

go 1.16

require (
	github.com/99designs/gqlgen v0.13.0 // indirect
	github.com/arangodb/go-driver v0.0.0-20210304082257-d7e0ea043b7f
	github.com/aws/aws-lambda-go v1.23.0
	github.com/awslabs/aws-lambda-go-api-proxy v0.9.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/spf13/viper v1.7.1
	github.com/vektah/gqlparser/v2 v2.1.0 // indirect
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
)
