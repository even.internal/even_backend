package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"github.com/naseem181997/even_backend/accounts/graph/generated"
	"github.com/naseem181997/even_backend/accounts/graph/model"
	"github.com/naseem181997/even_backend/accounts/internal/accounts"
)

func (r *entityResolver) FindAccountByKey(ctx context.Context, key *string) (*model.Account, error) {
	account := accounts.KeyInput{Key: *key}.GetAccountByKey(ctx)
	return &model.Account{Key: account.Key, Name: account.Name, Address: (*model.Address)(account.Address), NMachines: account.NMachines}, nil
}

// Entity returns generated.EntityResolver implementation.
func (r *Resolver) Entity() generated.EntityResolver { return &entityResolver{r} }

type entityResolver struct{ *Resolver }
