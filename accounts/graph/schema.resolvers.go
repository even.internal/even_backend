package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"time"

	"github.com/naseem181997/even_backend/accounts/graph/generated"
	"github.com/naseem181997/even_backend/accounts/graph/model"
	"github.com/naseem181997/even_backend/accounts/internal/accounts"
	"github.com/naseem181997/even_backend/accounts/internal/auth"
	"github.com/naseem181997/even_backend/accounts/internal/pkg/jwt"
)

func (r *mutationResolver) CreateAccount(ctx context.Context, input model.NewAccount) (string, error) {
	var account accounts.NewAccount
	account.Email = input.Email
	account.Name = input.Name
	account.Password = input.Password
	account.Address = accounts.Address{AddressLine: &input.Address.AddressLine, City: &input.Address.City, Region: input.Address.Region, PostalCode: &input.Address.PostalCode, Country: input.Address.Country}
	account.NMachines = input.NMachines
	account.IsDeleted = false
	Now := int(time.Now().Unix())
	account.TransactionDT = Now
	account.Save(ctx)
	token, err := jwt.GenerateToken(account.Email)
	if err != nil {
		return "", err
	}
	return token, nil
}

func (r *mutationResolver) UpdateAccount(ctx context.Context, input model.AccountInput) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	if accountKey == *input.Akey {
		var account accounts.AccountInput
		account.Key = input.Akey
		account.Email = input.Email
		account.Name = input.Name
		account.Password = input.Password
		account.Address = &accounts.Address{AddressLine: &input.Address.AddressLine, City: &input.Address.City, Region: input.Address.Region, PostalCode: &input.Address.PostalCode, Country: input.Address.Country}
		account.NMachines = input.NMachines
		myBool := false
		account.IsDeleted = &myBool
		Now := int(time.Now().Unix())
		account.TransactionDT = &Now
		Key := account.UpdateAccount(ctx)
		return &model.ReturnKey{Key: Key}, nil
	}
	return &model.ReturnKey{}, fmt.Errorf("access denied")
}

func (r *mutationResolver) DeleteAccount(ctx context.Context, input model.KeyInput) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	if accountKey == input.Key {
		var key accounts.KeyInput
		key.Key = input.Key
		Key := key.DeleteAccount(ctx)
		return &model.ReturnKey{Key: Key}, nil
	}
	return &model.ReturnKey{}, fmt.Errorf("access denied")
}

func (r *mutationResolver) Login(ctx context.Context, email string, password string) (string, error) {
	correct := accounts.Authenticate(ctx, email, password)
	if !correct {
		// 1
		return "", &accounts.WrongUsernameOrPasswordError{}
	}
	token, err := jwt.GenerateToken(email)
	if err != nil {
		return "", err
	}
	return token, nil
}

func (r *mutationResolver) RefreshToken(ctx context.Context, input model.RefreshTokenInput) (string, error) {
	email, err := jwt.ParseToken(input.Token)
	if err != nil {
		return "", fmt.Errorf("access denied")
	}
	token, err := jwt.GenerateToken(email)
	if err != nil {
		return "", err
	}
	return token, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

type mutationResolver struct{ *Resolver }
