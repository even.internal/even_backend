package main

import (
	"context"
	"log"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/awslabs/aws-lambda-go-api-proxy/gorillamux"
	"github.com/gorilla/mux"
	"github.com/naseem181997/even_backend/accounts/graph"
	"github.com/naseem181997/even_backend/accounts/graph/generated"
	"github.com/naseem181997/even_backend/accounts/internal/auth"
	database "github.com/naseem181997/even_backend/accounts/internal/pkg/db/arangodb"
)

var muxAdapter *gorillamux.GorillaMuxAdapter

func init() {
	r := mux.NewRouter()
	ctx := context.Background()

	r.Use(auth.Middleware(ctx))

	database.InitDB(ctx)
	//database.Migrate()
	schema := generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}})
	server := handler.NewDefaultServer(schema)
	r.Handle("/", server)
	muxAdapter = gorillamux.New(r)

}

func Handler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	rsp, err := muxAdapter.Proxy(req)
	if err != nil {
		log.Println(err)
	}
	return rsp, err
}

func main() {
	lambda.Start(Handler)
}
