package auth

import (
	"context"
	"net/http"
	"strings"

	"github.com/naseem181997/even_backend/accounts/internal/accounts"
	"github.com/naseem181997/even_backend/accounts/internal/pkg/jwt"
)

var accountCtxKey = &contextKey{"account"}

type contextKey struct {
	Key string
}

func Middleware(ctx context.Context) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			header := r.Header.Get("Authorization")
			// Allow unauthenticated users in
			if header == "" {
				next.ServeHTTP(w, r)
				return
			}
			//validate jwt token
			tokenStr := header
			splitToken := strings.Split(tokenStr, "Bearer ")
			tokenStr = splitToken[1]
			email, err := jwt.ParseToken(tokenStr)
			if err != nil {
				http.Error(w, "Invalid token", http.StatusForbidden)
				return
			}
			// create user and check if user exists in db
			key, err := accounts.GetAccountKeyByEmail(ctx, email)
			if err != nil {
				next.ServeHTTP(w, r)
				return
			}
			// put it in context
			ctx := context.WithValue(r.Context(), accountCtxKey, key)

			// and call the next with our new context
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

// ForContext finds the user from the context. REQUIRES Middleware to have run.
func ForContext(ctx context.Context) string {
	raw, _ := ctx.Value(accountCtxKey).(string)
	return raw
}
