package accounts

import (
	"context"
	"fmt"
	"log"

	driver "github.com/arangodb/go-driver"
	database "github.com/naseem181997/even_backend/accounts/internal/pkg/db/arangodb"
	"golang.org/x/crypto/bcrypt"
)

type NewAccount struct {
	Email         string
	Name          string
	Password      string
	Address       Address
	NMachines     int
	IsDeleted     bool
	TransactionDT int
}

type Address struct {
	AddressLine *string
	City        *string
	Region      *string
	PostalCode  *string
	Country     *string
}

type Account struct {
	Email         *string
	Key           *string
	Name          *string
	Address       *Address
	NMachines     *int
	IsDeleted     *bool
	TransactionDT *int
}

type AccountInput struct {
	Key           *string
	Email         *string
	Name          *string
	Password      *string
	Address       *Address
	NMachines     *int
	IsDeleted     *bool
	TransactionDT *int
}

type KeyInput struct {
	Key string
}

func (account NewAccount) Save(ctx context.Context) {
	col, err := database.DBSession.Collection(ctx, "accounts")
	if err != nil {
		log.Fatal(err)
	}
	account.Password, _ = HashPassword(account.Password)
	meta, err := col.CreateDocument(ctx, account)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Created document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
}

func (account AccountInput) UpdateAccount(ctx context.Context) string {
	var oldAcc *Account = &Account{}
	oldAcc.Key = account.Key

	col, err := database.DBSession.Collection(ctx, "accounts")
	if err != nil {
		log.Fatal(err)
	}
	*account.Password, _ = HashPassword(*account.Password)
	meta, err := col.UpdateDocument(driver.WithReturnOld(ctx, oldAcc), *account.Key, NewAccount{Email: *account.Email, Name: *account.Name, Password: *account.Password, Address: *account.Address, NMachines: *account.NMachines, IsDeleted: *account.IsDeleted, TransactionDT: *account.TransactionDT})
	if err != nil {
		log.Fatal(err)
	}
	temp_col, err := database.DBSession.Collection(ctx, "temp_accounts")
	if err != nil {
		log.Fatal(err)
	}
	temp_meta, err := temp_col.CreateDocument(ctx, oldAcc)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Saved document '%s' with key '%s', revision '%s' in temporal table\n", *oldAcc.Key, temp_meta.Key, meta.Rev)
	fmt.Printf("Updated document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}

func (key KeyInput) DeleteAccount(ctx context.Context) string {
	myBool := true
	var oldAcc *Account = &Account{}
	col, err := database.DBSession.Collection(ctx, "accounts")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.RemoveDocument(driver.WithReturnOld(ctx, oldAcc), key.Key)
	if err != nil {
		log.Fatal(err)
	}
	oldAcc.Key = &key.Key
	oldAcc.IsDeleted = &myBool
	temp_col, err := database.DBSession.Collection(ctx, "temp_accounts")
	if err != nil {
		log.Fatal(err)
	}
	temp_meta, err := temp_col.CreateDocument(ctx, oldAcc)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Saved document '%s' with key '%s', revision '%s' in temporal table\n", *oldAcc.Key, temp_meta.Key, meta.Rev)
	fmt.Printf("Deleted document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}
func GetAll(ctx context.Context) []Account {
	var accounts []Account
	query := `FOR acc IN accounts
				return acc`
	cursor, err := database.DBSession.Query(ctx, query, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close()
	for {
		var account Account
		meta, err := cursor.ReadDocument(ctx, &account)
		account.Key = &meta.Key
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
		accounts = append(accounts, account)
	}

	return accounts
}
func (key KeyInput) GetAccountByKey(ctx context.Context) *Account {
	var account *Account
	col, err := database.DBSession.Collection(ctx, "accounts")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.ReadDocument(ctx, key.Key, account)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
	return account
}

func GetAccountKeyByEmail(ctx context.Context, email string) (string, error) {
	var accounts []AccountInput
	query := `FOR acc IN accounts 
				FILTER acc.Email == @email
				return acc`
	bindVars := map[string]interface{}{
		"email": email,
	}
	cursor, err := database.DBSession.Query(ctx, query, bindVars)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close()
	for {
		var account AccountInput
		meta, err := cursor.ReadDocument(ctx, &account)
		account.Key = &meta.Key
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
		accounts = append(accounts, account)
	}
	if len(accounts) >= 1 {
		if *accounts[0].Email != email {
			return "", nil
		} else {
			return *accounts[0].Key, nil
		}
	}
	return "", nil
}

func Authenticate(ctx context.Context, email string, password string) bool {
	query := `FOR acc IN accounts 
				FILTER acc.Email == @email
				LIMIT 1
				return acc.Password`
	bindVars := map[string]interface{}{
		"email": email,
	}
	cursor, err := database.DBSession.Query(ctx, query, bindVars)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close()
	var AccountPwd string
	for {
		meta, err := cursor.ReadDocument(ctx, &AccountPwd)
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
	}
	return CheckPasswordHash(password, AccountPwd)
}

//HashPassword hashes given password
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

//CheckPassword hash compares raw password with it's hashed values
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
