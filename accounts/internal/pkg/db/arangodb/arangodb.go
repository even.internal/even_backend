package database

import (
	"context"
	"log"
	"os"

	driver "github.com/arangodb/go-driver"
	"github.com/arangodb/go-driver/http"

	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"fmt"
)

var DBSession driver.Database

func InitDB(ctx context.Context) {
	var db driver.Database
	db_opts := &driver.CreateDatabaseOptions{ /* ... */ }
	col_opts := &driver.CreateCollectionOptions{ /* ... */ }
	// Endpoint of the deployment
	endpoint := os.Getenv("ADB_ENDPOINT")
	// Root password
	rootPassword := os.Getenv("ABD_PWD")
	// Base64 encoded CA certificate
	encodedCA := os.Getenv("ADB_CA")
	// Decode CA certificate
	caCertificate, err := base64.StdEncoding.DecodeString(encodedCA)
	if err != nil {
		panic(err)
	}

	// Prepare TLS configuration
	tlsConfig := &tls.Config{}
	certpool := x509.NewCertPool()
	if success := certpool.AppendCertsFromPEM(caCertificate); !success {
		panic("Invalid certificate")
	}
	tlsConfig.RootCAs = certpool

	// Prepare HTTPS connection
	conn, err := http.NewConnection(http.ConnectionConfig{
		Endpoints: []string{endpoint},
		TLSConfig: tlsConfig,
	})
	if err != nil {
		panic(err)
	}

	// Create client
	opts := driver.ClientConfig{
		Connection:     conn,
		Authentication: driver.BasicAuthentication("root", rootPassword),
	}
	c, err := driver.NewClient(opts)
	if err != nil {
		panic(err)
	}

	// You're now connected
	// Note that ArangoDB Oasis runs deployments in a cluster configuration.
	// To achieve the best possible availability, your client application has to handle
	// connection failures by retrying operations if needed.

	version, err := c.Version(ctx)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Deployment is using version %s\n", version)

	db_exist, err := c.DatabaseExists(ctx, "even_dashboard")
	if err != nil {
		log.Fatalln(err.Error())
	}

	if !db_exist {
		db, err = c.CreateDatabase(ctx, "even_dashboard", db_opts)
		if err != nil {
			log.Fatalln(err.Error())
		}
	} else {
		db, err = c.Database(ctx, "even_dashboard")
		if err != nil {
			log.Fatalln(err.Error())
		}
	}

	col_exist, err := db.CollectionExists(ctx, "accounts")
	if err != nil {
		log.Fatalln(err.Error())
	}

	if !col_exist {
		_, err := db.CreateCollection(ctx, "accounts", col_opts)
		if err != nil {
			log.Fatalln(err.Error())
		}
	}

	temp_col_exist, err := db.CollectionExists(ctx, "temp_accounts")
	if err != nil {
		log.Fatalln(err.Error())
	}

	if !temp_col_exist {
		_, err := db.CreateCollection(ctx, "temp_accounts", col_opts)
		if err != nil {
			log.Fatalln(err.Error())
		}
	}
	DBSession = db
}
