module github.com/naseem181997/even_backend/stores

go 1.16

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/arangodb/go-driver v0.0.0-20210304082257-d7e0ea043b7f
	github.com/auth0/go-jwt-middleware v1.0.0
	github.com/aws/aws-lambda-go v1.23.0
	github.com/awslabs/aws-lambda-go-api-proxy v0.9.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/form3tech-oss/jwt-go v3.2.2+incompatible
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/rs/cors v1.7.0
	github.com/spf13/viper v1.7.1
	github.com/urfave/negroni v1.0.0
	github.com/vektah/gqlparser/v2 v2.1.0
)
