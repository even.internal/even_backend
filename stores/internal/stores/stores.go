package stores

import (
	"context"
	"fmt"
	"log"
	"reflect"
	"time"

	driver "github.com/arangodb/go-driver"
	"github.com/mitchellh/mapstructure"
	"github.com/naseem181997/even_backend/stores/graph/model"
	database "github.com/naseem181997/even_backend/stores/internal/pkg/db/arangodb"
)

func SaveStore(ctx context.Context, store map[string]interface{}) string {
	var VMKeys *[]string
	var dbVMs []map[string]interface{}
	col, err := database.DBSession.Collection(ctx, "stores")
	if err != nil {
		log.Fatal(err)
	}
	VMKeys = &[]string{}

	err = mapstructure.Decode(store["VendingMachines"].([]*model.NewVendingMachine), &dbVMs)
	if err != nil {
		panic(err)
	}
	for _, VM := range dbVMs {
		VM["AccountKey"] = store["AccountKey"]
		*VMKeys = append(*VMKeys, SaveVM(ctx, VM))
	}
	store["VendingMachines"] = VMKeys
	meta, err := col.CreateDocument(ctx, store)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Created document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}

func SaveVM(ctx context.Context, VM map[string]interface{}) string {
	var P *[]model.NewPlanogramPoint = &[]model.NewPlanogramPoint{}
	col, err := database.DBSession.Collection(ctx, "vending_machines")
	if err != nil {
		log.Fatal(err)
	}
	VM["Isdeleted"] = false
	VM["TransactionDT"] = int(time.Now().Unix())
	delete(VM, "Vmkey")
	err = mapstructure.Decode(VM["Planogram"], &P)
	if err != nil {
		log.Fatal(err)
	}
	VM["Planogram"] = P
	meta, err := col.CreateDocument(ctx, VM)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Created document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}

func SaveVMM(ctx context.Context, VMM map[string]interface{}) string {
	col, err := database.DBSession.Collection(ctx, "vending_machines_models")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.CreateDocument(ctx, VMM)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Created document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}

func UpdateStore(ctx context.Context, store map[string]interface{}) string {
	var VMKeys []string = []string{}
	var dbVMs []map[string]interface{}
	var oldStore *map[string]interface{} = &map[string]interface{}{}
	(*oldStore)["Skey"] = store["Skey"]
	err := mapstructure.Decode(store["VendingMachines"].([]*model.VendingMachineInput), &dbVMs)
	if err != nil {
		panic(err)
	}
	for _, VM := range dbVMs {
		var IsDeleted bool
		var Vmkey string
		err := mapstructure.Decode(VM["Isdeleted"], &IsDeleted)
		if err != nil {
			panic(err)
		}
		err = mapstructure.Decode(VM["Vmkey"], &Vmkey)
		if err != nil {
			panic(err)
		}
		if IsDeleted {
			DeleteVendingMachine(ctx, Vmkey)
		} else if Vmkey == "" {
			VM["AccountKey"] = store["AccountKey"]
			VMKeys = append(VMKeys, SaveVM(ctx, VM))
		} else {
			VMKeys = append(VMKeys, UpdateVendingMachine(ctx, VM))
		}
	}
	col, err := database.DBSession.Collection(ctx, "stores")
	if err != nil {
		log.Fatal(err)
	}
	var dbStore map[string]interface{}
	err = mapstructure.Decode(store, &dbStore)
	if err != nil {
		panic(err)
	}
	dbStore["Skey"] = nil
	dbStore["VendingMachines"] = VMKeys
	dbStore = deleteNilInterface(dbStore)
	meta, err := col.UpdateDocument(driver.WithReturnOld(ctx, oldStore), store["Skey"].(string), dbStore)
	if err != nil {
		log.Fatal(err)
	}
	temp_col, err := database.DBSession.Collection(ctx, "temp_stores")
	if err != nil {
		log.Fatal(err)
	}
	oldStore = cleanArangoDBMeta(oldStore)
	(*oldStore) = deleteNilInterface(*oldStore)
	temp_meta, err := temp_col.CreateDocument(ctx, oldStore)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Saved document '%s' with key '%s', revision '%s' in temporal table\n", (*oldStore)["Skey"], temp_meta.Key, meta.Rev)
	fmt.Printf("Updated document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}

func UpdateVendingMachine(ctx context.Context, VM map[string]interface{}) string {
	var oldVM *map[string]interface{} = &map[string]interface{}{}
	var Vmkey string
	var P *[]model.NewPlanogramPoint = &[]model.NewPlanogramPoint{}
	err := mapstructure.Decode(VM["Vmkey"], &Vmkey)
	if err != nil {
		log.Fatal(err)
	}
	(*oldVM)["Vmkey"] = Vmkey
	col, err := database.DBSession.Collection(ctx, "vending_machines")
	if err != nil {
		log.Fatal(err)
	}
	VM["Vmkey"] = nil
	VM["Isdeleted"] = false
	VM["TransactionDT"] = int(time.Now().Unix())
	err = mapstructure.Decode(VM["Planogram"], &P)
	if err != nil {
		log.Fatal(err)
	}
	VM["Planogram"] = P
	VM = deleteNilInterface(VM)
	meta, err := col.UpdateDocument(driver.WithReturnOld(ctx, oldVM), Vmkey, &VM)
	if err != nil {
		log.Fatal(err)
	}
	temp_col, err := database.DBSession.Collection(ctx, "temp_vending_machines")
	if err != nil {
		log.Fatal(err)
	}
	oldVM = cleanArangoDBMeta(oldVM)
	(*oldVM) = deleteNilInterface(*oldVM)
	temp_meta, err := temp_col.CreateDocument(ctx, oldVM)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Saved document '%s' with key '%s', revision '%s' in temporal table\n", (*oldVM)["Vmkey"], temp_meta.Key, meta.Rev)
	fmt.Printf("Updated document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}
func UpdateVMM(ctx context.Context, VMM map[string]interface{}) string {
	var oldVMM *map[string]interface{} = &map[string]interface{}{}
	(*oldVMM)["Vmmkey"] = VMM["Vmmkey"].(string)
	col, err := database.DBSession.Collection(ctx, "vending_machines_models")
	if err != nil {
		log.Fatal(err)
	}
	VMM["Vmmkey"] = nil
	VMM = deleteNilInterface(VMM)
	meta, err := col.UpdateDocument(driver.WithReturnOld(ctx, oldVMM), (*oldVMM)["Vmmkey"].(string), VMM)
	if err != nil {
		log.Fatal(err)
	}
	temp_col, err := database.DBSession.Collection(ctx, "temp_vending_machines_models")
	if err != nil {
		log.Fatal(err)
	}
	oldVMM = cleanArangoDBMeta(oldVMM)
	(*oldVMM) = deleteNilInterface(*oldVMM)
	temp_meta, err := temp_col.CreateDocument(ctx, oldVMM)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Saved document '%s' with key '%s', revision '%s' in temporal table\n", (*oldVMM)["Vmmkey"], temp_meta.Key, meta.Rev)
	fmt.Printf("Updated document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}
func DeleteStore(ctx context.Context, AccountKey string, key model.StoreKeyInput) string {
	var oldStore *map[string]interface{} = &map[string]interface{}{}
	if key.DeleteVM {
		oldVMs := GetStoreByKey(ctx, AccountKey, &key.Key)["VendingMachines"]
		var dbOldVMs []model.VendingMachine
		err := mapstructure.Decode(oldVMs, &dbOldVMs)
		if err != nil {
			log.Fatal(err)
		}
		for _, VM := range dbOldVMs {
			meta := DeleteVendingMachine(ctx, *(VM).Key)
			fmt.Printf("Deleted document with key '%s'\n", meta)
		}
	}
	col, err := database.DBSession.Collection(ctx, "stores")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.RemoveDocument(driver.WithReturnOld(ctx, oldStore), key.Key)
	if err != nil {
		log.Fatal(err)
	}
	(*oldStore)["Skey"] = &key.Key
	(*oldStore)["Isdeleted"] = &key.DeleteVM
	temp_col, err := database.DBSession.Collection(ctx, "temp_stores")
	if err != nil {
		log.Fatal(err)
	}
	oldStore = cleanArangoDBMeta(oldStore)
	(*oldStore) = deleteNilInterface(*oldStore)
	temp_meta, err := temp_col.CreateDocument(ctx, oldStore)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Saved document '%s' with key '%s', revision '%s' in temporal table\n", (*oldStore)["Skey"], temp_meta.Key, meta.Rev)
	fmt.Printf("Deleted document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}

func DeleteVendingMachine(ctx context.Context, key string) string {
	var oldVM *map[string]interface{} = &map[string]interface{}{}
	col, err := database.DBSession.Collection(ctx, "vending_machines")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.RemoveDocument(driver.WithReturnOld(ctx, oldVM), key)
	if err != nil {
		log.Fatal(err)
	}
	(*oldVM)["Vmkey"] = &key
	(*oldVM)["Isdeleted"] = true
	temp_col, err := database.DBSession.Collection(ctx, "temp_vending_machines")
	if err != nil {
		log.Fatal(err)
	}
	oldVM = cleanArangoDBMeta(oldVM)
	(*oldVM) = deleteNilInterface(*oldVM)
	temp_meta, err := temp_col.CreateDocument(ctx, oldVM)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Saved document '%s' with key '%s', revision '%s' in temporal table\n", (*oldVM)["Vmkey"], temp_meta.Key, meta.Rev)
	fmt.Printf("Deleted document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}
func DeleteVMM(ctx context.Context, key model.KeyInput) string {
	var oldVMM *map[string]interface{} = &map[string]interface{}{}
	col, err := database.DBSession.Collection(ctx, "vending_machines_models")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.RemoveDocument(driver.WithReturnOld(ctx, oldVMM), key.Key)
	if err != nil {
		log.Fatal(err)
	}
	(*oldVMM)["Vmmkey"] = &key.Key
	(*oldVMM)["Isdeleted"] = true
	temp_col, err := database.DBSession.Collection(ctx, "temp_vending_machines_models")
	if err != nil {
		log.Fatal(err)
	}
	oldVMM = cleanArangoDBMeta(oldVMM)
	(*oldVMM) = deleteNilInterface(*oldVMM)
	temp_meta, err := temp_col.CreateDocument(ctx, oldVMM)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Saved document '%s' with key '%s', revision '%s' in temporal table\n", (*oldVMM)["Vmmkey"], temp_meta.Key, meta.Rev)
	fmt.Printf("Deleted document with key '%s', revision '%s'\n", meta.Key, meta.Rev)
	return meta.Key
}
func GetAllStores(ctx context.Context, AccountKey string) []map[string]interface{} {
	var StoresOut *[]map[string]interface{} = &[]map[string]interface{}{}
	query := `FOR store IN stores
				FILTER store.AccountKey == @accountkey
				return store`
	bindVars := map[string]interface{}{
		"accountkey": AccountKey,
	}
	cursor, err := database.DBSession.Query(ctx, query, bindVars)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close()
	for {
		var store *map[string]interface{} = &map[string]interface{}{}
		var OutAddress *model.NewAddress = &model.NewAddress{}
		meta, err := cursor.ReadDocument(ctx, &store)
		(*store)["Key"] = &meta.Key
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		VMs := []model.VendingMachine{}
		var VMKeys []string
		err = mapstructure.Decode((*store)["VendingMachines"], &VMKeys)
		if err != nil {
			panic(err)
		}
		for _, VM := range VMKeys {
			dbVM := model.VendingMachine{}
			err = mapstructure.Decode(GetVMByKey(ctx, AccountKey, &VM), &dbVM)
			if err != nil {
				panic(err)
			}
			VMs = append(VMs, dbVM)
		}
		(*store)["VendingMachines"] = VMs
		err = mapstructure.Decode((*store)["Address"], &OutAddress)
		if err != nil {
			panic(err)
		}
		(*store)["Address"] = OutAddress
		fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
		*StoresOut = append(*StoresOut, *store)
	}

	return *StoresOut
}

func GetAllVMs(ctx context.Context, AccountKey string) []map[string]interface{} {
	var VMsOut *[]map[string]interface{} = &[]map[string]interface{}{}
	query := `FOR VM IN vending_machines
				FILTER VM.AccountKey == @accountkey
				return VM`
	bindVars := map[string]interface{}{
		"accountkey": AccountKey,
	}
	cursor, err := database.DBSession.Query(ctx, query, bindVars)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close()
	for {
		var VM *map[string]interface{} = &map[string]interface{}{}
		var dbPP []model.PlanogramPoint
		meta, err := cursor.ReadDocument(ctx, &VM)
		(*VM)["Key"] = &meta.Key

		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		var dbP []model.NewPlanogramPoint
		err = mapstructure.Decode((*VM)["Planogram"], &dbP)
		if err != nil {
			panic(err)
		}
		for _, PP := range dbP {
			var Prod model.Product
			var P model.PlanogramPoint
			err = mapstructure.Decode(GetProductByKey(ctx, AccountKey, &PP.Product), &Prod)
			if err != nil {
				panic(err)
			}
			P.Product = &Prod
			P.Position = PP.Position
			P.ActualQuantity = PP.ActualQuantity
			P.AddedQuantity = PP.AddedQuantity
			P.CreatedAt = PP.CreatedAt
			P.LastUpdatedAt = PP.LastUpdatedAt
			dbPP = append(dbPP, P)
		}
		fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
		(*VM)["Planogram"] = dbPP
		*VMsOut = append((*VMsOut), *VM)
	}

	return *VMsOut
}
func GetAllVMMs(ctx context.Context) []map[string]interface{} {
	var vmms []map[string]interface{}
	query := `FOR vmm IN vending_machines_models
				return vmm`

	cursor, err := database.DBSession.Query(ctx, query, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer cursor.Close()
	for {
		vmm := make(map[string]interface{})
		meta, err := cursor.ReadDocument(ctx, &vmm)
		vmm["Key"] = &meta.Key
		if driver.IsNoMoreDocuments(err) {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
		vmms = append(vmms, vmm)
	}

	return vmms
}
func GetStoreByKey(ctx context.Context, AccountKey string, key *string) map[string]interface{} {
	var store *map[string]interface{} = &map[string]interface{}{}
	col, err := database.DBSession.Collection(ctx, "stores")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.ReadDocument(ctx, *key, store)
	if err != nil {
		log.Fatal(err)
	}
	(*store)["Key"] = &meta.Key
	VMs := []model.VendingMachine{}
	var VMKeys []string
	err = mapstructure.Decode((*store)["VendingMachines"], &VMKeys)
	if err != nil {
		panic(err)
	}
	for _, VM := range VMKeys {
		dbVM := model.VendingMachine{}
		err = mapstructure.Decode(GetVMByKey(ctx, AccountKey, &VM), &dbVM)
		if err != nil {
			panic(err)
		}
		VMs = append(VMs, dbVM)
	}
	(*store)["VendingMachines"] = VMs
	fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
	return *store
}
func GetVMByKey(ctx context.Context, AccountKey string, key *string) map[string]interface{} {
	var VM *map[string]interface{} = &map[string]interface{}{}
	var dbPP []model.PlanogramPoint
	col, err := database.DBSession.Collection(ctx, "vending_machines")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.ReadDocument(ctx, *key, VM)
	if err != nil {
		log.Fatal(err)
	}
	(*VM)["Key"] = &meta.Key
	var dbP []model.NewPlanogramPoint
	err = mapstructure.Decode((*VM)["Planogram"], &dbP)
	if err != nil {
		panic(err)
	}
	for _, PP := range dbP {
		var Prod model.Product
		var P model.PlanogramPoint
		err = mapstructure.Decode(GetProductByKey(ctx, AccountKey, &PP.Product), &Prod)
		if err != nil {
			panic(err)
		}
		P.Product = &Prod
		P.Position = PP.Position
		P.ActualQuantity = PP.ActualQuantity
		P.AddedQuantity = PP.AddedQuantity
		P.CreatedAt = PP.CreatedAt
		P.LastUpdatedAt = PP.LastUpdatedAt
		dbPP = append(dbPP, P)
	}
	(*VM)["Planogram"] = dbPP
	fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
	return *VM
}
func GetProductByKey(ctx context.Context, AccountKey string, key *string) map[string]interface{} {
	var product *map[string]interface{} = &map[string]interface{}{}
	col, err := database.DBSession.Collection(ctx, "products")
	if err != nil {
		log.Fatal(err)
	}
	meta, err := col.ReadDocument(ctx, *key, product)
	if err != nil {
		log.Fatal(err)
	}
	(*product)["Key"] = &meta.Key
	fmt.Printf("Got doc with key '%s' from query\n", meta.Key)
	return *product
}

func IsNilFixed(i interface{}) bool {
	if i == nil {
		return true
	}
	switch reflect.TypeOf(i).Kind() {
	case reflect.Ptr, reflect.Map, reflect.Array, reflect.Chan, reflect.Slice:
		return reflect.ValueOf(i).IsNil()
	}
	return false
}
func cleanArangoDBMeta(i *map[string]interface{}) *map[string]interface{} {
	(*i)["_key"] = nil
	(*i)["_id"] = nil
	(*i)["_rev"] = nil
	return i
}
func deleteNilInterface(i map[string]interface{}) map[string]interface{} {
	for key, value := range i {
		if IsNilFixed(value) {
			fmt.Printf("deleting %v=>%v\n", key, value)
			delete(i, key)
		}
	}
	return i
}
