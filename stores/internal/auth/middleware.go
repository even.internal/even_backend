package auth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	"github.com/form3tech-oss/jwt-go"
	"github.com/urfave/negroni"
)

type Response struct {
	Message string `json:"message"`
}

type Jwks struct {
	Keys []JSONWebKeys `json:"keys"`
}

type JSONWebKeys struct {
	Kty string   `json:"kty"`
	Kid string   `json:"kid"`
	Use string   `json:"use"`
	N   string   `json:"n"`
	E   string   `json:"e"`
	X5c []string `json:"x5c"`
}

type contextKey struct {
	Key string
}

var accountCtxKey = &contextKey{"sub"}

func Middleware() *jwtmiddleware.JWTMiddleware {
	return jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			// Verify 'aud' claim
			if aInterface, ok := token.Claims.(jwt.MapClaims)["aud"].([]interface{}); ok {
				aString := make([]string, len(aInterface))
				for i, v := range aInterface {
					aString[i] = v.(string)
				}
				token.Claims.(jwt.MapClaims)["aud"] = aString
			} else {
				token.Claims.(jwt.MapClaims)["aud"] = []string{token.Claims.(jwt.MapClaims)["aud"].(string)}
			}
			aud := os.Getenv("AUTH0_AUDIENCE")
			checkAud := token.Claims.(jwt.MapClaims).VerifyAudience(aud, false)
			if !checkAud {
				return token, errors.New("Invalid audience.")
			}
			// Verify 'iss' claim
			iss := os.Getenv("AUTH0_DOMAIN")
			checkIss := token.Claims.(jwt.MapClaims).VerifyIssuer(iss, false)
			if !checkIss {
				return token, errors.New("Invalid issuer.")
			}
			cert, err := getPemCert(token)
			if err != nil {
				panic(err.Error())
			}
			result, _ := jwt.ParseRSAPublicKeyFromPEM([]byte(cert))
			return result, nil

		},
		SigningMethod: jwt.SigningMethodRS256,
	})
}

func CheckScope(ctx context.Context) negroni.Handler {
	return negroni.HandlerFunc(func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		authHeaderParts := strings.Split(r.Header.Get("authorization"), " ")
		tokenString := authHeaderParts[1]
		scope := "full_access"
		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			cert, err := getPemCert(token)
			if err != nil {
				return nil, err
			}
			result, _ := jwt.ParseRSAPublicKeyFromPEM([]byte(cert))
			return result, nil
		})
		if err != nil {
			fmt.Printf("%v\n", err)
			http.Error(w, "Invalid Token", http.StatusForbidden)
			return
		}
		claims, ok := token.Claims.(jwt.MapClaims)
		hasScope := false
		if ok && token.Valid {
			result := strings.Split(claims["scope"].(string), " ")
			for i := range result {
				if result[i] == scope {
					hasScope = true
				}
			}
		}
		if !hasScope {
			message := "Insufficient scope."
			http.Error(w, message, http.StatusForbidden)
			return
		}

		ctx = context.WithValue(ctx, accountCtxKey, claims["sub"])
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})

}

func getPemCert(token *jwt.Token) (string, error) {
	cert := ""
	resp, err := http.Get(os.Getenv("AUTH0_DOMAIN") + ".well-known/jwks.json")

	if err != nil {
		return cert, err
	}
	defer resp.Body.Close()

	var jwks = Jwks{}
	err = json.NewDecoder(resp.Body).Decode(&jwks)

	if err != nil {
		return cert, err
	}

	for k := range jwks.Keys {
		if token.Header["kid"] == jwks.Keys[k].Kid {
			cert = "-----BEGIN CERTIFICATE-----\n" + jwks.Keys[k].X5c[0] + "\n-----END CERTIFICATE-----"
		}
	}

	if cert == "" {
		err := errors.New("Unable to find appropriate key.")
		return cert, err
	}

	return cert, nil
}

// ForContext finds the user from the context. REQUIRES Middleware to have run.
func ForContext(ctx context.Context) string {
	raw, _ := ctx.Value(accountCtxKey).(string)
	return raw
}
