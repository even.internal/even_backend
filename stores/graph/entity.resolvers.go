package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"github.com/mitchellh/mapstructure"
	"github.com/naseem181997/even_backend/stores/graph/generated"
	"github.com/naseem181997/even_backend/stores/graph/model"
	"github.com/naseem181997/even_backend/stores/internal/auth"
	"github.com/naseem181997/even_backend/stores/internal/stores"
)

func (r *entityResolver) FindStoreByKey(ctx context.Context, key *string) (*model.Store, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.Store{}, fmt.Errorf("access denied")
	}
	var resultStore *model.Store
	dbStore := stores.GetStoreByKey(ctx, accountKey, key)
	err := mapstructure.Decode(dbStore, &resultStore)
	if err != nil {
		panic(err)
	}
	return resultStore, nil
}

// Entity returns generated.EntityResolver implementation.
func (r *Resolver) Entity() generated.EntityResolver { return &entityResolver{r} }

type entityResolver struct{ *Resolver }
