package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/naseem181997/even_backend/stores/graph/generated"
	"github.com/naseem181997/even_backend/stores/graph/model"
	"github.com/naseem181997/even_backend/stores/internal/auth"
	"github.com/naseem181997/even_backend/stores/internal/stores"
)

func (r *mutationResolver) CreateStore(ctx context.Context, input model.NewStore) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	var dbStore map[string]interface{}
	var dbAddress *model.NewAddress = &model.NewAddress{}
	err := mapstructure.Decode(input, &dbStore)
	if err != nil {
		panic(err)
	}
	dbStore["AccountKey"] = accountKey
	dbStore["Isdeleted"] = false
	dbStore["TransactionDT"] = int(time.Now().Unix())
	err = mapstructure.Decode(dbStore["Address"], &dbAddress)
	if err != nil {
		panic(err)
	}
	dbStore["Address"] = *dbAddress
	Key := stores.SaveStore(ctx, dbStore)
	return &model.ReturnKey{Key: Key}, nil
}

func (r *mutationResolver) UpdateStore(ctx context.Context, input model.StoreInput) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	var dbStore map[string]interface{}

	err := mapstructure.Decode(input, &dbStore)
	if err != nil {
		panic(err)
	}
	dbStore["AccountKey"] = accountKey
	dbStore["Isdeleted"] = false
	dbStore["TransactionDT"] = int(time.Now().Unix())

	Key := stores.UpdateStore(ctx, dbStore)
	return &model.ReturnKey{Key: Key}, nil
}

func (r *mutationResolver) DeleteStore(ctx context.Context, input model.StoreKeyInput) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	Key := stores.DeleteStore(ctx, accountKey, input)
	return &model.ReturnKey{Key: Key}, nil
}

func (r *mutationResolver) CreateVMModel(ctx context.Context, input model.NewVendingMachineModel) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	var dbVMM map[string]interface{}
	err := mapstructure.Decode(input, &dbVMM)
	if err != nil {
		panic(err)
	}
	dbVMM["Isdeleted"] = false
	dbVMM["TransactionDT"] = int(time.Now().Unix())
	Key := stores.SaveVMM(ctx, dbVMM)
	return &model.ReturnKey{Key: Key}, nil
}

func (r *mutationResolver) UpdateVMModel(ctx context.Context, input model.VendingMachineModelInput) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	var dbVMM map[string]interface{}
	err := mapstructure.Decode(input, &dbVMM)
	if err != nil {
		panic(err)
	}
	dbVMM["Isdeleted"] = false
	dbVMM["TransactionDT"] = int(time.Now().Unix())
	Key := stores.UpdateVMM(ctx, dbVMM)
	return &model.ReturnKey{Key: Key}, nil
}

func (r *mutationResolver) DeleteVMModel(ctx context.Context, input model.KeyInput) (*model.ReturnKey, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return &model.ReturnKey{}, fmt.Errorf("access denied")
	}
	Key := stores.DeleteVMM(ctx, input)
	return &model.ReturnKey{Key: Key}, nil
}

func (r *queryResolver) Stores(ctx context.Context) ([]*model.Store, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return []*model.Store{}, fmt.Errorf("access denied")
	}
	var resultStores []*model.Store
	dbStores := stores.GetAllStores(ctx, accountKey)
	err := mapstructure.Decode(dbStores, &resultStores)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v", &resultStores)
	return resultStores, nil
}

func (r *queryResolver) VendingMachineModels(ctx context.Context) ([]*model.VendingMachineModel, error) {
	accountKey := auth.ForContext(ctx)
	if accountKey == "" {
		return []*model.VendingMachineModel{}, fmt.Errorf("access denied")
	}
	var resultVMMs []*model.VendingMachineModel
	dbVMMs := stores.GetAllVMMs(ctx)
	err := mapstructure.Decode(dbVMMs, &resultVMMs)
	if err != nil {
		panic(err)
	}
	return resultVMMs, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
