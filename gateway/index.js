const serverless = require('serverless-http');
const { ApolloServer } = require("apollo-server-express");
const { ApolloGateway,RemoteGraphQLDataSource  } = require("@apollo/gateway");
const graphiql = require("graphql-playground-middleware-express").default;
const express = require("express");
const jwt = require('express-jwt');
const jwtAuthz = require('express-jwt-authz');
const jwksRsa = require('jwks-rsa');

require('dotenv').config()

if (!process.env.AUTH0_DOMAIN || !process.env.AUTH0_AUDIENCE) {
  throw 'Make sure you have AUTH0_DOMAIN, and AUTH0_AUDIENCE in your .env file';
}

const app = express();
app.use(
  jwt({
    // Dynamically provide a signing key based on the [Key ID](https://tools.ietf.org/html/rfc7515#section-4.1.4) header parameter ("kid") and the signing keys provided by the JWKS endpoint.
    secret: jwksRsa.expressJwtSecret({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: `https://${process.env.AUTH0_DOMAIN}/.well-known/jwks.json`
    }),
  
    // Validate the audience and the issuer.
    audience: process.env.AUTH0_AUDIENCE,
    issuer: [`https://${process.env.AUTH0_DOMAIN}/`],
    algorithms: ['RS256']
  })
);

const gateway = new ApolloGateway({
  buildService({ name, url }) {
    return new RemoteGraphQLDataSource({
      url,
      willSendRequest({ request, context }) {
        request.http.headers.set(
          "authorization",
          context.auth ? context.auth : ""
        );
      }
    });
}
});

const corsOptions = {
  origin: "*",
  credentials: true
};

const server = new ApolloServer({
  gateway,
  subscriptions: false,
  context: ({ req }) => {
    const auth = req.headers.authorization || null;
    return { auth };
  },
  path: "/graphql"
});
app.get("/playground", graphiql({ endpoint: "/dev/graphql" }));
server.applyMiddleware({ app,cors: corsOptions });

exports.graphqlHandler = serverless(app)